#!/usr/bin/env python3

"""## Task

Write code to find a string of characters that contains only letters from

`acdegilmnoprstuw`

such that the hash(the_string) is

`930846109532517`

if hash is defined by the following pseudo-code:

```
Int64 hash (String s) {
    Int64 h = 7
    String letters = "acdegilmnoprstuw"
    for(Int32 i = 0; i < s.length; i++) {
        h = (h * 37 + letters.indexOf(s[i]))
    }
    return h
}
```

For example, if we were trying to find the 7 letter string where
`hash(the_string)` was `680131659347`, the answer would be "leepadg". """

VALID_LETTERS = "acdegilmnoprstuw"
MULTIPLIER = 37


def encrypt(message):
    """Implements the pseudo-code below

        Int64 hash (String s) {
            Int64 h = 7
            String letters = "acdegilmnoprstuw"
            for(Int32 i = 0; i < s.length; i++) {
                h = (h * 37 + letters.indexOf(s[i]))
            }
            return h
        }
    """
    hash_value = 7
    for char in message:
        hash_value = (hash_value * MULTIPLIER) + VALID_LETTERS.index(char)
    return hash_value


def decrypt(hashed):
    """

    Tries to generate the message encrypted using the function defined above"""
    message = []
    while hashed != 7:
        index = hashed % MULTIPLIER
        message.insert(0, VALID_LETTERS[index])
        hashed //= MULTIPLIER
        assert hashed >= 7
    return ''.join(message)


if __name__ == '__main__':
    hashed_message = 930846109532517
    unhashed = decrypt(hashed_message)
    assert hashed_message == encrypt(unhashed)
