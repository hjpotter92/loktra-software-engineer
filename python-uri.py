#!/usr/bin/env python3

"""## Python URI task

In Python, write a class or module with a bunch of functions for manipulating a
URI. For this exercise, pretend that the urllib, urllib2, and urlparse modules
don't exist. You can use other standard Python modules, such as re, for this.
The focus of the class or module you write should be around usage on the web, so
you'll want to have things that make it easier to update or append a querystring
var, get the scheme for a URI, etc., and you may want to include ways to figure
out the domain for a URL ([british-site.co.uk](http://british-site.co.uk), [us-
site.com](http://us-site.com), [mailto:
yourname@example.com](mailto:yourname@example.com), etc.)

We're looking for correctness (you'll probably want to read the relevant RFCs;
make sure you handle edge cases), and elegance of your API (does it let you do
the things you commonly want to do with URIs in a really straightforward way?,)
as well as coding style. If you don't know Python already, then this is also an
exercise in learning new things quickly and well. Your code should be well-
commented and documented and conform to the guidelines in the PEP 8 Style Guide
for Python Code. Include some instructions and examples of usage in your
documentation. You may also want to write unit tests."""

import re

# The regex below is something I wrote 4 years ago on regex101.com
# available at https://regex101.com/library/kX8hK8
COMPLETE_REGEX_PATTERN = r"^(?:(?P<scheme>(?:http|ftp|irc)s?)://)?(?:(?P<user>[^\s:@]+):?(?P<password>[^@\s]+)?@)?(?P<host>(?:www\.)?(?:[^:/\s]+)(?::(?P<port>\d+))?)(?P<request>/?[^?#\s]+)?\??(?P<query>[^#\s]*)?\#?(?P<anchor>\S*)?$"
COMPILED_REGEX = re.compile(COMPLETE_REGEX_PATTERN)
URI_PART_DEFAULTS = {
    'scheme': 'http',
    'user': None,
    'password': None,
    'host': '',
    'port': None,
    'request': '/',
    'query': None,
    'anchor': None
}


class URI:
    """Executes the regex above for URI"""
    def __init__(self, url):
        super(URI, self).__init__()
        self._uri = url
        match = COMPILED_REGEX.match(url)
        if match:
            for part, default in URI_PART_DEFAULTS.items():
                setattr(self, part, match.group(part) or default)

    def _parse_qso(self, querystring):
        query = querystring.split('&')
        if ''.join(query) == querystring:
            query = querystring.split(';')
        query_parts = [tuple(part.split('=')) for part in query]
        qso = {
            key: value for key, value in query_parts
        }
        setattr(self, '_qso', qso)

    def __setattr__(self, name, value):
        if name is 'query' and value:
            self._parse_qso(value)
        if name in URI_PART_DEFAULTS:
            name = '_' + name
        if name.startswith('_'):
            self.__dict__[name] = value

    def __getattr__(self, name):
        if name in URI_PART_DEFAULTS:
            if name is 'query':
                return self.get_query()
            return self.__dict__['_' + name]
        elif name in self.__dict__:
            return self.__dict__[name]

    def get_query(self):
        if self._qso is None:
            return ''
        return '&'.join([f"{key}={value}" for key, value in self._qso.items()])

    def add_query_param(self, param, value):
        if self._qso is None:
            setattr(self, '_qso', {})
        self._qso[param] = value

    def remove_query_param(self, param):
        if self._qso is None:
            return False
        return self._qso.pop(param, None)

    def compile(self):
        password = f":{self.password}" if self.password is not None else ""
        user_pass = f"{self.user}{password}@" if self.user else ""
        query = self.get_query()
        if query:
            query = f"?{query}"
        anchor = f"#{self.anchor}" if self.anchor else ""
        host = f"{user_pass}{self.host}"
        request = f"{self.request}{query}{anchor}"
        return f"{self.scheme}://{host}{request}"


if __name__ == '__main__':
    TEST_CASES = (
        "http://user:pass@some.example.com:80/directory/sub/place?C=M;O=A#nth",
        "http://another.example.com/some/path/to/a/file.bin?request=true;part=1#somewhere",
        "https://www.example.com",
        "ircs://example.server.net:6697/#channel",
        "irc://irc.example.org/channel",
        "ftp://www.place.example.gov/",
        "www.places.to.be",
        "somewhere.to/go",
        "british-site.co.uk",
        "a@b.c/d?e=f&g=h#i"
    )
    for case in TEST_CASES:
        uri = URI(case)
        # various URI parameters can be accessed directly via their names
        # like `uri.scheme`, `uri.query` etc
        uri.add_query_param('host', 'thing')
        print(uri.host, uri.compile())
