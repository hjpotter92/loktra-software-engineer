# Custom Hash decryption

> Write code to find a string of characters that contains only letters from
>
> `acdegilmnoprstuw`
>
> such that the hash(the_string) is
>
> `930846109532517`
>
> if hash is defined by the following pseudo-code:
>
> ```
> Int64 hash (String s) {
>     Int64 h = 7
>     String letters = "acdegilmnoprstuw"
>     for(Int32 i = 0; i < s.length; i++) {
>         h = (h * 37 + letters.indexOf(s[i]))
>     }
>     return h
> }
> ```

Implemented in the `decrypt.py` for both `encrypt` and `decrypt` methods. The `assert` statements will raise an exception if invalid hash was provided to `decrypt` method call.

# Python URI class module

> In Python, write a class or module with a bunch of functions for manipulating a
> URI. For this exercise, pretend that the urllib, urllib2, and urlparse modules
> don't exist. You can use other standard Python modules, such as re, for this.
> The focus of the class or module you write should be around usage on the web, so
> you'll want to have things that make it easier to update or append a querystring
> var, get the scheme for a URI, etc., and you may want to include ways to figure
> out the domain for a URL ([british-site.co.uk](http://british-site.co.uk), [us-
> site.com](http://us-site.com), [mailto:
> yourname@example.com](mailto:yourname@example.com), etc.)

> We're looking for correctness (you'll probably want to read the relevant RFCs;
> make sure you handle edge cases), and elegance of your API (does it let you do
> the things you commonly want to do with URIs in a really straightforward way?,)
> as well as coding style. If you don't know Python already, then this is also an
> exercise in learning new things quickly and well. Your code should be well-
> commented and documented and conform to the guidelines in the PEP 8 Style Guide
> for Python Code. Include some instructions and examples of usage in your
documentation. You may also want to write unit tests.

Implemented in the `python-uri.py` file. Currently does not support the
`mailto:` scheme, but does support all of ftp/http/irc protocols. The various
aspects of an URI (listed below) can be simply modified by setting those
attributes on the object itself.

To get the properly generated URI, call the `.compile()` method, which returns
the parsed URI as a string value. For manipulation of the query string, the
`add_query_param` and `remove_query_param` are provided.

The attributes of URI are:

 * scheme
 * user
 * password
 * host
 * port
 * request
 * query
 * anchor
